import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ressource_relationnel/controller/login/login_controller.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/vue/user/page/connexion_page.dart';
import 'package:ressource_relationnel/vue/user/page/register_page.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class ProfilNonConnecte extends StatefulWidget {
  @override
  _ProfilNonConnecteState createState() => _ProfilNonConnecteState();
}

class _ProfilNonConnecteState extends State<ProfilNonConnecte> {
  LoginController loginController;

  @override
  void initState() {
    loginController = LoginController(
        Provider.of<RepositoryUserAbstract>(context, listen: false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height:  MediaQuery.of(context).size.height * 0.23,
          decoration: BoxDecoration(
            color: Color(0xff44dcc0),
            borderRadius: BorderRadius.vertical(
                bottom: Radius.elliptical(
                    MediaQuery.of(context).size.width, 150.0)),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top : 50.0),
            child: AutoSizeText(
              "Bienvenue dans Ressource Relationnel ! Pour l'instant, vous n'avez pas encore de compte, vous ne pouvez pas utiliser toutes les fonctionnalitées de l'application. "
                  .i18n,
              maxLines: 3,
              textAlign: TextAlign.center,
              style: GoogleFonts.alice(fontWeight: FontWeight.bold,color: Colors.white),
            ),
          ),
        ),
        SizedBox(height: 25,),
        Image(
          image: AssetImage('assets/images/ressource_relationnelle.png'),
          height: MediaQuery.of(context).size.height * 0.20,
        ),
        SizedBox(height: 25,),
        Container(
          child: Expanded(
            child: Column(
              children: [
                RaisedButton(
                  child: Text(
                    "Se connecter",
                    style: GoogleFonts.alice(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ConnexionPage()));
                  },
                ),
                SizedBox(height: 4,),
                RaisedButton(
                  child: Text(
                    "Créer un compte".i18n,
                    style: GoogleFonts.alice(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => RegisterPage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
