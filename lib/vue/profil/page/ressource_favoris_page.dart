import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/ressource_favoris/ressource_favoris_controller.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class RessourceFavorisPage extends StatelessWidget {
  RessourceFavorisController ressourceFavorisController =
      RessourceFavorisController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: AutoSizeText(
            "Vos ressources favorites".i18n,
            maxLines: 2,
          )),
          Expanded(
            child: ListView.builder(
              itemCount: ressourceFavorisController.listeRessource.length,
              itemBuilder: (context, index) {
                return Container(
                  child: Text("coucou".i18n),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
