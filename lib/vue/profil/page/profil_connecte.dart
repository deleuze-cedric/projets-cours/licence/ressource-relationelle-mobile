import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/vue/profil/page/ressource_favoris_page.dart';
import 'package:ressource_relationnel/vue/profil/widget/actualite_ressource_partage.dart';
import 'package:ressource_relationnel/vue/profil/widget/avatar.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class ProfilConnecte extends StatefulWidget {
  @override
  _ProfilConnecteState createState() => _ProfilConnecteState();
}

class _ProfilConnecteState extends State<ProfilConnecte> {
  CoreController coreController;

  @override
  void initState() {
    coreController = Provider.of<CoreController>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              border: BorderDirectional(
                  bottom: BorderSide(
                width: 1,
                color: Colors.transparent,
              )),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff44dcc0),
                  Color(0xff64c7c7),
                ],
              ),
            ),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.40,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Card(
                      elevation: 35,
                      shape: CircleBorder(),
                      child: CircleAvatar(
                        radius: 55,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AutoSizeText(
                      "${coreController.user.prenom} ${coreController.user.nom.toUpperCase()}",
                      style: TextStyle(color: Colors.white),
                    ),
                    Card(
                      margin:
                          EdgeInsets.symmetric(horizontal: 35, vertical: 10),
                      elevation: 35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.30,
                            margin: EdgeInsets.all(10),
                            child: FlatButton(
                              child: AutoSizeText(
                                "Voir les ressources favorites".i18n,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.black),
                              ),
                              padding: EdgeInsets.all(10),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        RessourceFavorisPage(),
                                  ),
                                );
                              },
                              color: Colors.white,
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.30,
                            margin: EdgeInsets.all(10),
                            child: FlatButton(
                              child: AutoSizeText(
                                "Changer la photo de profil".i18n,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.black),
                              ),
                              padding: EdgeInsets.all(10),
                              onPressed: () {},
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: ActualiteRessourcePartage(user: coreController.user),
        ),
      ],
    );
  }
}
