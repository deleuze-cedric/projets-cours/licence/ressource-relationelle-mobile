import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/ressource_partage/ressource_controller.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/profil/widget/ressource_card.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class ActualiteRessourcePartage extends StatefulWidget {
  User user;


  ActualiteRessourcePartage({this.user});

  @override
  _ActualiteRessourcePartageState createState() =>
      _ActualiteRessourcePartageState();
}

class _ActualiteRessourcePartageState extends State<ActualiteRessourcePartage> {
  RessourceController ressourceController;

  @override
  void initState() {
    ressourceController = RessourceController(
        repositoryRessource:Provider.of<RepositoryRessourceAbstract>(context, listen: false),
        currentUser:widget.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return LoadingStack(
          message: ressourceController.message,
          visible: ressourceController.isLoading,
          body: ressourceController.error
              ? Center(
                  child: Text(ressourceController.errorMessage),
                )
              : ressourceController.listRessource.length == 0
                  ? Center(
                      child: AutoSizeText("Vous n'avez de ressource".i18n),
                    )
                  : ListView.separated(
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                      itemCount: ressourceController.listRessource.length,
                      itemBuilder: (context, index) {
                        return RessourceCard(
                            ressourceController.listRessource[index]);
                      }),
        );
      },
    );
  }
}
