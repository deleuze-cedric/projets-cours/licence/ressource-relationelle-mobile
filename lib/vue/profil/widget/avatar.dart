import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/user/user_controller.dart';
import 'package:ressource_relationnel/main.i18n.dart';


class Avatar extends StatefulWidget {
  @override
  _AvatarState createState() => _AvatarState();
}

class _AvatarState extends State<Avatar> {
  UserController userController;
  @override
  void initState() {
    userController = Provider.of<UserController>(context,listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Column(
          children: [
            userController.image == null
                ? CircleAvatar(
                    radius: 80,
                    backgroundColor: Colors.blue,
                  )
                : CircleAvatar(
                radius: 80,
                  child: ClipRect(
                    child: Image.file(

                        userController.image,
                        fit: BoxFit.cover,
                      ),
                  ),
                ),
            GestureDetector(
              onTap: userController.getImage,
              child: Text(
                "Changer la photo de profil".i18n,
                style: TextStyle(
                    fontSize: 10,
                    color: Colors.blue,
                    decoration: TextDecoration.underline),
              ),
            )
          ],
        );
      },
    );
  }
}
