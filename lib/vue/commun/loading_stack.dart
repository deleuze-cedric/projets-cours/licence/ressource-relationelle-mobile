import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:logger/logger.dart';
class LoadingStack extends StatelessWidget {
  Logger logger = new Logger();
  final Widget _body;
  final bool _visible;
  final String _message;
  LoadingStack({Key key, @required Widget body, @required bool visible, @required String message})
      : assert(body != null),
        assert(visible != null),
        assert(message != null),
        _body = body,
        _visible = visible,
        _message = message,
        super(key: key);
  @override
  Widget build(BuildContext context) {
    logger.v("_visible $_visible");
    return Container(
      child: new Stack(
        children: <Widget>[
          _body,
          if (_visible) ...[
            Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
            Center(
              child: ConstrainedBox(
                constraints: new BoxConstraints(
                  maxWidth: 400,
                ),
                child: new Container(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  margin: EdgeInsets.symmetric(horizontal: 48),
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        child: new Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Center(
                              child: SizedBox(
                                height: 60.0,
                                width: 60.0,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor: new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              margin: const EdgeInsets.only(left: 8, right: 8, top: 15.0),
                              child: new Center(
                                child: AutoSizeText(
                                  _message,
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]
        ],
      ),
    );
  }
}

