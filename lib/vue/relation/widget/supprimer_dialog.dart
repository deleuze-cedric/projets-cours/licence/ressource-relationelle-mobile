import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/main.i18n.dart';

Future<void> SupprimerDialog(
    {BuildContext context, var controller, Relation relation}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title:
            Text("Êtes vous sur de vouloir supprimer votre ressource ?".i18n),
        actions: [
          TextButton(
            child: Text(
              'Retour'.i18n,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          TextButton(
            child: Text(
              'Accepter'.i18n,
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () {
              controller.effacerDemande(relation).then((e) {
                if (e) {
                  Navigator.pop(context);
                  Flushbar(
                    messageText: Text(
                      "La relation a correctement été effacée",
                      style: TextStyle(color: Colors.green),
                    ),
                    duration: Duration(seconds: 1),
                  )..show(context);
                } else {
                  Navigator.pop(context);
                  Flushbar(
                    title: "Une erreur s'est malheuresement produite".i18n,
                    messageText: Text(
                      "La relation a correctement été effacée".i18n,
                      style: TextStyle(color: Colors.red),
                    ),
                    duration: Duration(seconds: 2),
                  )..show(context);
                }
              });
            },
          ),
        ],
      );
    },
  );
}
