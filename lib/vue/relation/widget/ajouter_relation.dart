import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/type_relation/type_relation_controller.dart';
import 'package:ressource_relationnel/controller/user/user_controller.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/relation/widget/validation_dialog.dart';
import 'package:ressource_relationnel/main.i18n.dart';

Future<void> AjouterRelation(
    BuildContext context, TypeRelationController typeRelationController) async {
  UserController userController =
      Provider.of<UserController>(context, listen: false);
  userController.getListUsers();
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Ajouter une relation'.i18n),
        content: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          height: MediaQuery.of(context).size.height * 0.85,
          child: SingleChildScrollView(
            child: Observer(
              builder: (_) {
                return LoadingStack(
                  message: typeRelationController.message,
                  visible: typeRelationController.isLoading,
                  body: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.50,
                          child: SearchBar<User>(
                            minimumChars: 1,
                            onSearch: userController.filterListUsers,
                            hintText: "Rechercher...".i18n,
                            onItemFound: (User user, int index) {
                              return ListTile(
                                leading: CircleAvatar(
                                  backgroundColor: Colors.blue,
                                ),
                                title: AutoSizeText(
                                  user.prenom,
                                  maxLines: 1,

                                ),
                                subtitle: AutoSizeText(
                                  user.nom,
                                  maxLines: 1,
                                ),
                                trailing: Icon(Icons.add),
                                onTap: () => ValidationDialog(
                                    context: context,
                                    targetUser: user,
                                    currentUser: userController.currentUser,
                                    typeRelation: typeRelationController
                                        .selectedTypeRelation),
                              );
                            },
                          ),
                        ),
                        ListTile(
                          title: AutoSizeText(
                            "Type de relation :".i18n,
                            maxLines: 1,
                          ),
                          subtitle: DropdownButton(
                              value: typeRelationController.selectedTypeRelation,
                              items: typeRelationController.listeTypeRelation
                                  .map((TypeRelation typeRelation) {
                                return DropdownMenuItem<TypeRelation>(
                                  value: typeRelation,
                                  child: AutoSizeText(
                                    typeRelation.libelle,
                                    maxLines: 1,
                                  ),
                                );
                              }).toList(),
                              onChanged: (value) {
                                typeRelationController.changeSelectedItem(value);
                              }),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        /*SizedBox(
                          height: MediaQuery.of(context).size.height * 0.75,
                          child: UserList(),
                        ),*/
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Revenir en arrière'.i18n),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
