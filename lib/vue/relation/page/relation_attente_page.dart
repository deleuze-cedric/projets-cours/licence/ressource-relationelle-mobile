import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/relation_attente/relation_attente_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/modele_api/etat_relation_api/etat_relation_api.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class RelationAttentePage extends StatefulWidget {
  User currentUser;

  RelationAttentePage({this.currentUser});

  @override
  _RelationAttentePageState createState() => _RelationAttentePageState();
}

class _RelationAttentePageState extends State<RelationAttentePage> {
  RelationAttenteController relationAttenteController;

  @override
  void initState() {
    relationAttenteController = RelationAttenteController(
        Provider.of<RepositoryRelationAbstract>(context, listen: false),
        widget.currentUser);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return LoadingStack(
          message: relationAttenteController.message,
          visible: relationAttenteController.isLoading,
          body: ListView.builder(
            itemCount: relationAttenteController.listRelation.length,
            itemBuilder: (context, index) {
              var item = relationAttenteController.listRelation[index];
              return relationAttenteController.listRelation.isEmpty
                  ? Center(
                      child: Container(
                        child: Text(
                            "Vous n'avez pas encore fait de demande de relation".i18n),
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.grey[400], width: 0.5),
                        color: Colors.white,
                      ),
                      child: Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        child: Container(
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.blue,
                            ),
                            title: AutoSizeText(
                              '${item.demandeur.prenom} ${item.demandeur.nom}',
                              maxLines: 2,
                            ),
                            subtitle: AutoSizeText(
                              'Demande une relation de type : ${item.typeRelation.libelle}',
                              maxLines: 3,
                            ),
                          ),
                        ),
                        secondaryActions: <Widget>[
                          IconSlideAction(
                            caption: 'Accepter',
                            color: Colors.green,
                            icon: Icons.check,
                            onTap: () {
                              EtatRelationApi etat = EtatRelationApi(true);
                              relationAttenteController.decisionDemande(
                                  relation: item, etat: etat);
                            },
                          ),
                          IconSlideAction(
                            caption: 'Refuser'.i18n,
                            color: Colors.red,
                            icon: Icons.cancel_outlined,
                            onTap: () {
                              EtatRelationApi etat = EtatRelationApi(false);
                              relationAttenteController.decisionDemande(
                                  relation: item, etat: etat);
                            },
                          ),
                        ],
                      ),
                    );
            },
          ),
        );
      },
    );
  }
}
