import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/controller/relation/relation_controller.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class RelationConnectPage extends StatefulWidget {
  @override
  _RelationConnectPageState createState() => _RelationConnectPageState();
}

class _RelationConnectPageState extends State<RelationConnectPage> {
  RelationController relationController;
  CoreController coreController;

  @override
  void initState() {
    coreController = Provider.of<CoreController>(context, listen: false);
    relationController = RelationController(
        Provider.of<RepositoryRelationAbstract>(context, listen: false),
        coreController.user);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return LoadingStack(
        visible: relationController.isLoading,
        message: relationController.message,
        body: ListView.builder(
          itemCount: relationController.listRelation.length,
          itemBuilder: (context, index) {
            var item = relationController.listRelation[index];
            return relationController.listRelation.length == 0
                ? Center(
                    child: Container(
                      child: Text("Vous n'avez pas encore de relations".i18n),
                    ),
                  )
                : Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.grey[400], width: 0.5),
                      color: Colors.white,
                    ),
                    child: Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      actionExtentRatio: 0.25,
                      child: Container(
                        child: ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.blue,
                          ),
                          title: AutoSizeText(
                            '${item.demandeur.prenom} ${item.demandeur.nom}',
                            maxLines: 2,
                          ),
                          subtitle: AutoSizeText(
                            'Demande une relation de type : ${item.typeRelation.libelle}',
                            maxLines: 3,
                          ),
                        ),
                      ),
                    ),
                  );
          },
        ),
      );
    });
  }
}
