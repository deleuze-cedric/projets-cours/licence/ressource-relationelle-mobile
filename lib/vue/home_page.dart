import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/navigation/navigation_controller.dart';

class HomePage extends StatelessWidget {
  NavigationController navigationController;

  @override
  Widget build(BuildContext context) {
    navigationController =
        Provider.of<NavigationController>(context, listen: false);
    return Observer(builder: (_) {
      return Scaffold(

        body: IndexedStack(
          index: navigationController.tabItemSelected,
          children: navigationController.listPages,
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.white,
          backgroundColor: Colors.white,
          unselectedItemColor: Colors.white,
          currentIndex: navigationController.tabItemSelected,
          onTap: (index) {
            navigationController.changeTabSelected(index);
          },
          items: [
            BottomNavigationBarItem(

              backgroundColor: Color(0xff44dcc0),
              icon: Icon(Icons.home),
              label: "profil",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color(0xff44dcc0),
              icon: Icon(Icons.account_balance_wallet_rounded),
              label: "Ressources",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color(0xff44dcc0),
              icon: Icon(Icons.accessibility_new_sharp),
              label: "Actualité",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color(0xff44dcc0),
              icon: Icon(Icons.account_box),
              label: "Relations",
            ),
            BottomNavigationBarItem(
              backgroundColor: Color(0xff44dcc0),
              icon: Icon(Icons.settings),
              label: "Paramètre",
            ),
          ],
        ),
      );
    });
  }
}
