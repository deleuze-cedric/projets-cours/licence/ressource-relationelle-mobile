import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ressource_relationnel/controller/actualite/actualite_disconnect_controller.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/vue/actualite/widget/action_bouton.dart';
import 'package:ressource_relationnel/vue/actualite/widget/contenu_ressource.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'commentary_page.dart';

class FilActualiteNonConnectePage extends StatefulWidget {
  @override
  _FilActualiteNonConnectePageState createState() =>
      _FilActualiteNonConnectePageState();
}

class _FilActualiteNonConnectePageState
    extends State<FilActualiteNonConnectePage> {
  ActualiteDisonnectController actualiteDisonnectController;

  @override
  void initState() {
    actualiteDisonnectController = new ActualiteDisonnectController(
        Provider.of<RepositoryRessourceAbstract>(context, listen: false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return LoadingStack(
        visible: actualiteDisonnectController.isLoading,
        message: actualiteDisonnectController.message,
        body: actualiteDisonnectController.error
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(actualiteDisonnectController.errorMessage),
                    IconButton(
                        icon: Icon(Icons.replay),
                        onPressed: () =>
                            actualiteDisonnectController.getListRessource()),
                  ],
                ),
              )
            : ListView.builder(
                itemCount: actualiteDisonnectController.listRessource.length,
                itemBuilder: (context, index) {
                  return Card(
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: [
                        ListTile(
                          leading: GestureDetector(
                            child: CircleAvatar(
                              radius: 20,
                              backgroundColor: Colors.blue,
                            ),
                          ),
                          title: AutoSizeText(
                            '${actualiteDisonnectController.listRessource[index].titre}',
                            maxLines: 2,
                          ),
                          subtitle: AutoSizeText(
                            '${actualiteDisonnectController.listRessource[index].createur.nom.toUpperCase()} ${actualiteDisonnectController.listRessource[index].createur.prenom}',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: ContenuRessource(actualiteDisonnectController.listRessource[index]),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            height: 200,
                            child: Icon(FontAwesomeIcons.fileImage)),
                        ButtonBar(
                          alignment: MainAxisAlignment.start,
                          children: [
                            FlatButton(
                              textColor: Color(0xff44dcc0),
                              onPressed: () {},
                              child: ActionBouton(actualiteDisonnectController.listRessource[index]),
                            ),
                            FlatButton(
                              textColor: Color(0xff44dcc0),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            CommentaryPage()));
                              },
                              child: AutoSizeText('Commenter'.i18n, maxLines: 1),
                            ),
                            IconButton(
                              icon: Icon(Icons.attachment),
                              onPressed: () {},
                              color: Color(0xff44dcc0),
                            ),
                            IconButton(
                              icon: Icon(Icons.assistant_direction),
                              onPressed: () {},
                              color: Color(0xff44dcc0),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
      );
    });
  }
}
