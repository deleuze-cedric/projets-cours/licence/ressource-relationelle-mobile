import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';

class ContenuRessource extends StatelessWidget {
  Ressource ressource;

  ContenuRessource(this.ressource);

  @override
  Widget build(BuildContext context) {
    switch (ressource.contenu.keys.toString()) {
      case "(url)":
        {
          return AutoSizeText(
            "Cliquer sur visionner pour accéder à la ressource.",
          );
        }
        break;

      case "(text)":
        {
          return Html(
            data: ressource.contenu["text"],
          );
        }
        break;

      case "(text, url)":
        {
          return Column(
            children: [
              Html(
                data: ressource.contenu["text"],
              ),
            ],
          );
        }
        break;
      case "(url, media)":
        {
          return SizedBox();
        }
    }

    if (ressource.contenu.keys.toString() == "(url)") {
      return AutoSizeText(
        ressource.contenu["url"],
        style: TextStyle(color: Colors.black.withOpacity(0.6)),
      );
    } else
      return Container(
        child: Text(ressource.contenu.keys.toString()),
      );
  }
}
