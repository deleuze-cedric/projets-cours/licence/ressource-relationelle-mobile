import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/controller/categorie_ressource/categorie_ressource_controller.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/ressources/page/add_ressource/ajouter_ressource_page.dart';

class CategorieRessourcePage extends StatelessWidget {
  CategorieRessourceController categorieRessourceController;
  RessourceFormController ressourceFormController;

  CategorieRessourcePage(
      this.categorieRessourceController, this.ressourceFormController);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingStack(
        message: categorieRessourceController.message,
        visible: categorieRessourceController.isLoading,
        body: categorieRessourceController.error
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AutoSizeText(categorieRessourceController.errorMessage),
                    IconButton(
                      icon: Icon(Icons.replay),
                      onPressed: () => categorieRessourceController
                          .getListCategorieRessource(),
                    ),
                  ],
                ),
              )
            : Column(
                children: [
                  Expanded(
                    child: GridView.builder(
                      itemCount: categorieRessourceController
                          .listCategorieRessource.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            ressourceFormController.categorieRessourceId =
                                categorieRessourceController
                                    .listCategorieRessource[index].id;
                            Navigator.push(context, MaterialPageRoute<void>(
                                builder: (BuildContext context) {
                              return AjouterRessourcePage(
                                  ressourceFormController);
                            }));
                          },
                          child: Card(
                            child:Stack(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/ressource_relationnelle.png'),
                                    fit: BoxFit.fitHeight,
                                    alignment: Alignment.topCenter,
                                  ),
                                  Container(
                                    child: Text(
                                      categorieRessourceController
                                          .listCategorieRessource[index].libelle,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),

                                ])
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
      ),
    );
  }
}

