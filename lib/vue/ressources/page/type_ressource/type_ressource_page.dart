import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/categorie_ressource/categorie_ressource_controller.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';
import 'package:ressource_relationnel/controller/type_ressource/type_ressource_controller.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/ressources/page/categorie_ressource/categorie_ressource_page.dart';

class TypeRessourcePage extends StatelessWidget {
  TypeRessourceController typeRessourceController;
  CategorieRessourceController categorieRessourceController;

  TypeRessourcePage(this.typeRessourceController, this.categorieRessourceController);

  @override
  Widget build(BuildContext context) {
    RessourceFormController ressourceFormController = Provider.of<RessourceFormController>(context, listen: false);
    return Observer(
      builder: (_) {
        return LoadingStack(
          visible: typeRessourceController.isLoading,
          message: typeRessourceController.message,
          body: typeRessourceController.error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AutoSizeText(typeRessourceController.errorMessage),
                      IconButton(
                        icon: Icon(Icons.replay),
                        onPressed: () => typeRessourceController.getListTypeRessource(),
                      ),
                    ],
                  ),
                )
              : Column(
                  children: [
                    Expanded(
                      child: GridView.builder(
                        itemCount: typeRessourceController.listTypeRessource.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                        ),
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              ressourceFormController.typeRessourceId = typeRessourceController.listTypeRessource[index].id;
                              Navigator.push(context, MaterialPageRoute<void>(builder: (BuildContext context) {
                                return CategorieRessourcePage(categorieRessourceController, ressourceFormController);
                              }));
                            },
                            child: Card(
                                child: Stack(
                                    children: <Widget>[
                                      Image(
                                        image: AssetImage(
                                            'assets/images/ressource_relationnelle.png'),
                                        fit: BoxFit.cover,
                                        alignment: Alignment.topCenter,
                                      ),
                                  Container(
                                    child: Text(
                                      typeRessourceController
                                          .listTypeRessource[index].libelle,
                                    ),
                                  ),

                                ])),
                          );
                        },
                      ),
                    )
                  ],
                ),
        );
      },
    );
  }
}
