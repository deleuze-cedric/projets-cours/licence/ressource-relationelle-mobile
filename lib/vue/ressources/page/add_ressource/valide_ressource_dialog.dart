import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:ressource_relationnel/controller/ressource_form/ressource_form_controller.dart';

Future<void> ValideRessourceDialog(
    {BuildContext context,
    RessourceFormController ressourceFormController}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      final _formKey = GlobalKey<FormBuilderState>();
      return AlertDialog(
        title: Text('Renseigner le titre, valider et le tour est joué !'),
        content: FormBuilder(
          key: _formKey,
          child: FormBuilderTextField(
            attribute: "titre",
            decoration: InputDecoration(hintText: "Votre titre..."),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Retour'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text('Enregistrer'),
            onPressed: () {
              _formKey.currentState.save();
              ressourceFormController.titre =
                  _formKey.currentState.value["titre"];
              //ressourceFormController.creationRessource();
            },
          ),
        ],
      );
    },
  );
}
