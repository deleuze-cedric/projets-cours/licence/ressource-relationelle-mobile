import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/vue/profil/widget/actualite_ressource_partage.dart';

// Page de profil d'un autre utilisateur

class UserProfil extends StatefulWidget {
  final User user;

  const UserProfil(this.user);

  @override
  _UserProfilState createState() => _UserProfilState();
}

class _UserProfilState extends State<UserProfil> {
  User user;

  @override
  void initState() {
    user = widget.user;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                border: BorderDirectional(
                    bottom: BorderSide(
                  width: 1,
                  color: Colors.transparent,
                )),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xff44dcc0),
                    Color(0xff64c7c7),
                  ],
                ),
              ),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.40,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Card(
                        elevation: 35,
                        shape: CircleBorder(),
                        child: CircleAvatar(
                          radius: 55,
                          backgroundColor: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AutoSizeText(
                        "${user.prenom} ${user.nom.toUpperCase()}",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: ActualiteRessourcePartage(
              user: widget.user,
            ),
          ),
        ],
      ),
    );
  }
}
