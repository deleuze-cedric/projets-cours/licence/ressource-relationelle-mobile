import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/controller/login/login_controller.dart';
import 'package:ressource_relationnel/controller/user/user_controller.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/user/page/connexion_auto_alert.dart';
import 'package:ressource_relationnel/vue/home_page.dart';
import 'package:ressource_relationnel/main.i18n.dart';

class ConnexionPage extends StatefulWidget {
  @override
  _ConnexionPageState createState() => _ConnexionPageState();
}

class _ConnexionPageState extends State<ConnexionPage> {
  final GlobalKey<FormBuilderState> connexionKey =
      GlobalKey<FormBuilderState>();

  LoginController loginController;

  CoreController coreController;

  UserController userController;

  bool saveUser = true;

  @override
  void initState() {
    loginController = LoginController(
        Provider.of<RepositoryUserAbstract>(context, listen: false));
    coreController = Provider.of<CoreController>(context, listen: false);
    userController = Provider.of<UserController>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Scaffold(
          body: LoadingStack(
            message: loginController.msg,
            visible: loginController.authentificationIsLoading,
            body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FormBuilder(
                    key: connexionKey,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 16.0,top:45),
                          child: Text(
                            "Se connecter".i18n,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 22),
                          ),
                        ),
                        ListTile(
                          leading: const Icon(Icons.mail),
                          title: FormBuilderTextField(
                            attribute: "email",
                            decoration: InputDecoration(hintText: "Mail".i18n),
                            validators: [
                              FormBuilderValidators.email(),
                              FormBuilderValidators.required(),
                            ],
                          ),
                        ),
                        ListTile(
                          leading: const Icon(Icons.vpn_key),
                          title: FormBuilderTextField(
                            showCursor: true,
                            obscureText: true,
                            attribute: "mdp",
                            decoration: InputDecoration(hintText: "Mot de passe".i18n),
                          ),
                        ),
                        SizedBox(height: 35,),
                        AutoSizeText(
                            "Enregistrer l'utilisateur pour une prochaine connexion ?".i18n,maxLines: 2,textAlign: TextAlign.center,),
                        Checkbox(
                          value: saveUser,
                          activeColor: Colors.green,
                          onChanged: (value) {
                            setState(() {
                              saveUser = value;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: RaisedButton(
                      child: Text(
                        "Se connecter".i18n,
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.green,
                      onPressed: () {
                        connexionKey.currentState.save();
                        loginController
                            .authentification(connexionKey.currentState)
                            .then((value) {
                          if (value) {
                            print("Email, connexion_page : " + loginController.email);
                            print("Password, connexion_page : " + loginController.password);
                            // Connection false
                            Flushbar(
                              title: "Une erreur s'est malheuresement produite".i18n,
                              message: loginController.errorMsg,
                              duration: Duration(seconds: 3),
                            )..show(context);
                          } else {
                            // Connection est ok on récupère l'user
                            loginController.saveUser();
                            coreController.setIsConnect(true);
                            coreController.setUser(loginController.user);
                            userController.setCurrentUser(coreController.user);
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage()),
                                (Route<dynamic> route) => false);
                          }
                        });
                      },
                    ),
                  ),
                  RaisedButton(
                      child: Text(
                        "Connexion rapide".i18n,
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.orange,
                      onPressed: () {
                        return ListConnectionAutoAlert(context, loginController, coreController, userController);
                      }),
                  Padding(
                    padding: const EdgeInsets.only(top: 45),
                    child: Image(
                      image:
                          AssetImage('assets/images/ressource_relationnelle.png'),
                      height: MediaQuery.of(context).size.height * 0.20,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
