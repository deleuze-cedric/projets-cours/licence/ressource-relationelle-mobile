import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ressource_relationnel/controller/core/core_controller.dart';
import 'package:ressource_relationnel/controller/login/login_controller.dart';
import 'package:ressource_relationnel/controller/user/user_controller.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/vue/commun/loading_stack.dart';
import 'package:ressource_relationnel/vue/home_page.dart';


Future<void> ListConnectionAutoAlert(
    BuildContext context, LoginController loginController, CoreController coreController, UserController userController) async {
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Observer(builder: (_) {
          return AlertDialog(
            content: Container(
              height: 300,
              width: 200,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey[300],
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(8),
              ),
              child: loginController.listUser.isEmpty
                  ? Center(
                child: Text("Aucun utilisateur enregistré".i18n),
              )
                  : LoadingStack(
                visible: loginController.authentificationIsLoading,
                message: loginController.msg,
                body: ListView.builder(
                  itemCount: loginController.listUser.length,
                  itemBuilder: (context, i) {
                    return Column(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            await loginController
                                .authentification_auto(
                              loginController.listUser[i].email,
                              loginController.listUser[i].password,
                            )
                                .then(
                                  (value) {
                                if (!value) {

                                  coreController.setIsConnect(true);
                                  coreController.setUser(loginController.user);
                                  userController.setCurrentUser(coreController.user);
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()),
                                          (Route<dynamic> route) => false);

                                } else {
                                  Flushbar(
                                    title: "Une erreur s'est malheuresement produite".i18n,
                                    message: loginController.errorMsg,
                                    duration: Duration(seconds: 3),
                                  )
                                    ..show(context);
                                }
                              },
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 8,
                            ),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Icon(
                                  Icons.arrow_right,
                                  color: Colors.blue,
                                ),
                                Expanded(
                                  child: AutoSizeText(
                                    '${loginController.listUser[i].email}',
                                    maxLines: 1,
                                  ),
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    loginController.removeUserSaveAtIndex(i);

                                    Flushbar(
                                      title: "Utilisateur supprimé.",
                                      message:
                                      "L'utilisateur a été supprimé avec succès.",
                                      duration: Duration(seconds: 5),
                                    )
                                      ..show(context);
                                    //DeleteSaveStoreAlert(
                                    //    context, loginController, i);
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        if (i != loginController.listUser.length - 1)
                          Divider(
                            color: Colors.grey[300],
                            height: 5,
                            thickness: 1.5,
                            indent: 15,
                            endIndent: 15,
                          ),
                      ],
                    );
                  },
                ),
              ),
            ),
          );
        });
      });
}