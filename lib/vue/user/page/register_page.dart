import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:ressource_relationnel/controller/user_form/user_form_controller.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/vue/user/page/connexion_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormBuilderState> registerKey = GlobalKey<FormBuilderState>();

  UserFormController userFormController;

  @override
  void initState() {
    userFormController = UserFormController(
        Provider.of<RepositoryUserAbstract>(context, listen: false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FormBuilder(
              key: registerKey,
              child: Column(
                children: [
                  Text(
                    "Créer un compte",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  ),
                  Padding(padding: const EdgeInsets.only(bottom: 15.0)),
                  ListTile(
                    leading: const Icon(Icons.person_outline_sharp),
                    title: FormBuilderTextField(
                      attribute: "nom",
                      decoration: InputDecoration(hintText: "Nom"),
                      validators: [FormBuilderValidators.required()],
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.person_outline_sharp),
                    title: FormBuilderTextField(
                      attribute: "prenom",
                      decoration: InputDecoration(hintText: "Prenom"),
                      validators: [FormBuilderValidators.required()],
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.person_outline_sharp),
                    title: FormBuilderTextField(
                      attribute: "pseudo",
                      decoration:
                      InputDecoration(hintText: "Pseudo (optionnel)"),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.alternate_email),
                    title: FormBuilderTextField(
                      attribute: "mail",
                      decoration: InputDecoration(hintText: "Mail"),
                      validators: [
                        FormBuilderValidators.email(),
                        FormBuilderValidators.required(),
                      ],
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.vpn_key),
                    title: FormBuilderTextField(
                      attribute: "mdp",
                      decoration: InputDecoration(hintText: "Mot de passe"),
                      obscureText: true,
                      validators: [
                        FormBuilderValidators.required(),
                      ],
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.check),
                    title: FormBuilderTextField(
                      attribute: "verif_mdp",
                      decoration: InputDecoration(
                          hintText: "Confirmer le mot de passe"),
                      obscureText: true,
                      validators: [
                        FormBuilderValidators.required(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(padding: const EdgeInsets.only(top: 25.0)),
            RaisedButton(
              child: Text(
                "Valider",
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.green,
              onPressed: () {
                if (registerKey.currentState.saveAndValidate()) {
                  registerKey.currentState.save();
                  if (registerKey.currentState.value["mdp"] ==
                  registerKey.currentState.value["verif_mdp"]) {
                    userFormController.register(registerKey.currentState);

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConnexionPage()),
                            (Route<dynamic> route) => false);
                  }
                  else{
                    Flushbar(
                      title:  "Mot de passe incorrect",
                      message: "Le mot de passe et la confirmation ne sont pas identiques",
                      duration:  Duration(seconds: 5),
                    )..show(context);
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
