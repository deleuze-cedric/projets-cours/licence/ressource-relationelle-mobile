import 'dart:convert';

import 'package:ressource_relationnel/modele/categorie_ressource/categorie_ressource.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';
import 'package:ressource_relationnel/modele/result.dart';
import 'package:ressource_relationnel/modele/type_ressource/type_ressource.dart';
import 'package:dio/dio.dart';
import 'package:ressource_relationnel/modele_api/ressource_api/ressource_api.dart';

import 'package:ressource_relationnel/stock_const.dart';

import 'package:ressource_relationnel/main.i18n.dart';

class DataSourceRessource {
  Future<Result<List<TypeRessource>>> getListTypeRessource() async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.get("${url_api}type_ressources");
      var pageInatedJSON = jsonDecode(response.data);
      var listTypeRess = pageInatedJSON["hydra:member"]
          .map((e) => TypeRessource.fromJson(e))
          .cast<TypeRessource>()
          .toList();
      return Result(listTypeRess);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<CategorieRessource>>> getListCategorieRessource() async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.get("${url_api}categorie_ressources");
      var pageInatedJSON = jsonDecode(response.data);
      var listCategorieRessource = pageInatedJSON["hydra:member"]
          .map((e) => CategorieRessource.fromJson(e))
          .cast<CategorieRessource>()
          .toList();
      return Result(listCategorieRessource);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<Ressource>>> getListRessourceDeUser(
      String token, String uuid) async {
    try {
      var dio = new Dio();
      if (token != null) {
        dio.options.headers["authorization"] = "Bearer $token";
      }
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.get("${url_api}users/$uuid/liste_ressources");
      var jsonResponse = jsonDecode(response.data);
      var listRessource = jsonResponse["hydra:member"]
          .map((e) => Ressource.fromJson(e))
          .cast<Ressource>()
          .toList();
      return Result(listRessource);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    }
    catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  // List des ressource pour un utilisateur non connecté
  Future<Result<List<Ressource>>> getListRessource() async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.get("${url_api}ressources");
      var jsonResponse = jsonDecode(response.data);
      var listRessource = jsonResponse["hydra:member"]
          .map((e) => Ressource.fromJson(e))
          .cast<Ressource>()
          .toList();
      return Result(listRessource);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  // List de la fil d'actualité pour l'utilisateru connecté
  Future<Result<List<Ressource>>> getListRessourceConnect(String token) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers['authorization'] = "Bearer $token";

      Response response = await dio.get("${url_api}ressources");

      var jsonResponse = jsonDecode(response.data);
      print(jsonResponse);
      var listRessource = jsonResponse["hydra:member"]
          .map((e) => Ressource.fromJson(e))
          .cast<Ressource>()
          .toList();
      print(listRessource);
      return Result(listRessource);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<List<Ressource>>> getListRessourceFavoris(String token) async {
    try {
      var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      dio.options.headers['authorization'] = "Bearer $token";

      Response response = await dio.get("${url_api}ressources");

      var jsonResponse = jsonDecode(response.data);
      var listRessource = jsonResponse["hydra:member"]
          .map((e) => Ressource.fromJson(e))
          .cast<Ressource>()
          .toList();
      return Result(listRessource);
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }

  Future<Result<bool>> ajouterRessource(RessourceApi ressourceApi) async {
    try {
      /*var dio = new Dio();
      dio.options.headers['content-Type'] = "application/json";
      Response response = await dio.get("${url_api}ressources");
      var jsonResponse = jsonDecode(response.data);
      var listRessource = jsonResponse["hydra:member"]
          .map((e) => Ressource.fromJson(e))
          .cast<Ressource>()
          .toList();
      return Result(listRessource);*/
    } on DioError catch (e) {
      if (e.response == null) {
        return Result.error("Vous n'êtes pas connecté à internet".i18n);
      }
      if (e.response.statusCode == 401) {
        return Result.error("Login ou mot de passe faux".i18n);
      }
    } catch (e) {
      return Result.error("Il y a un problème !");
    }
  }
}
