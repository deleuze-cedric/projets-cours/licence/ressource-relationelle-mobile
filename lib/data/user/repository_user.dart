import 'package:ressource_relationnel/modele/result.dart';

import 'data_source_user.dart';
import 'dart:io';
import 'package:ressource_relationnel/modele/user/user.dart';

abstract class RepositoryUserAbstract {
  Future<Result<User>> authentification(String email, String password);

  Future<User> register(User user);

  Future uploadImage(File image);

  Future<Result<User>> getUserProfil(User user, User currentUser);

  Future<Result<List<User>>> getUserList(User user);

  Future<Result<List<User>>> getUserListFilter(User user,String filtre);
}

class RepositoryUser extends RepositoryUserAbstract {
  DataSourceUser dataSourceUser = new DataSourceUser();

  Future<Result<User>> authentification(String email, String password) async {
    print(email);
    print(password);
    var userResult = await dataSourceUser.authentification(email, password);
    if (userResult.isSuccess()) {
      return Result(userResult.result);
    } else {
      return Result.error(userResult.errorMessage);
    }
  }

  Future<User> register(User user) async {
    var userResult = await dataSourceUser.register(user);
    if (userResult.isSuccess()) {
      User user = userResult.result;
      return user;
    }
  }

  Future uploadImage(File image) async =>
      await dataSourceUser.uploadImage(image);

  Future<Result<User>> getUserProfil(User user, User currentUser) async {
    var userResult = await dataSourceUser.getProfilUser(user, currentUser);
    if (userResult.isSuccess()) {
      return Result(userResult.result);
    } else {
      return Result.error(userResult.errorMessage);
    }
  }

  @override
  Future<Result<List<User>>> getUserList(User user) async {
    var userListResult = await dataSourceUser.getListUser(user);
    if (userListResult.isSuccess()) {
      return Result(userListResult.result);
    } else {
      return Result.error(userListResult.errorMessage);
    }
  }

  @override
  Future<Result<List<User>>> getUserListFilter(User user,String filtre) async {
    var userListResult = await dataSourceUser.getListUserFilter(filtre, user);
    if (userListResult.isSuccess()) {
      return Result(userListResult.result);
    } else {
      return Result.error(userListResult.errorMessage);
    }
  }
}
