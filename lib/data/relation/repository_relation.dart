import 'package:ressource_relationnel/data/relation/data_source_relation.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/modele/result.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/modele_api/etat_relation_api/etat_relation_api.dart';

abstract class RepositoryRelationAbstract {
  Future<Result<List<Relation>>> getListRelation(
    User user,
  );

  Future<Result<List<TypeRelation>>> getListTypeRelation();

  Future<Result<bool>> ajouterRelation({
    User currentUser,
    User targetUser,
    TypeRelation typeRelation,
  });

  // Les demandes que l'utilisateur en cours de connexion a envoyé
  Future<Result<List<Relation>>> getListMesDemandes({
    User currentUser,
  });

  Future<Result<bool>> effacerRelation({
    Relation relation,
    String token,
  });

  // Liste des demandes de relations qu'a reçu l'utilisateur connecté

  Future<Result<List<Relation>>> getListDemandesEnAttente({
    User currentUser,
  });

  Future<Result<bool>> decisionRelation({
    Relation relation,
    String token,
    EtatRelationApi etat,
  });
}

class RepositoryRelation extends RepositoryRelationAbstract {
  DataSourceRelation dataSourceRelation = new DataSourceRelation();

  Future<Result<List<Relation>>> getListRelation(User user) async {
    var listRelation = await dataSourceRelation.getListRelation(user);
    if (listRelation.isSuccess()) {
      return Result(listRelation.result);
    } else {
      return Result.error(listRelation.errorMessage);
    }
  }

  @override
  Future<Result<List<TypeRelation>>> getListTypeRelation() async {
    var listTypeRelation = await dataSourceRelation.getListTypeRelation();
    if (listTypeRelation.isSuccess()) {
      return Result(listTypeRelation.result);
    } else {
      return Result.error(listTypeRelation.errorMessage);
    }
  }

  @override
  Future<Result<bool>> ajouterRelation(
      {User currentUser, User targetUser, TypeRelation typeRelation}) async {
    var isOk = await dataSourceRelation.ajouterRelation(
        currentUser: currentUser,
        targetUser: targetUser,
        typeRelation: typeRelation);
    if (isOk.isSuccess()) {
      return Result(isOk.result);
    } else {
      return Result.error(isOk.errorMessage);
    }
  }

  @override
  Future<Result<List<Relation>>> getListMesDemandes({User currentUser}) async {
    var listResult = await dataSourceRelation.getListMesDemandes(
      currentUser: currentUser,
    );
    if (listResult.isSuccess()) {
      return Result(listResult.result);
    } else {
      return Result.error(listResult.errorMessage);
    }
  }

  @override
  Future<Result<bool>> effacerRelation(
      {Relation relation, String token}) async {
    var requestResult = await dataSourceRelation.effacerRelation(
        relation: relation, token: token);
    if (requestResult.isSuccess()) {
      return Result(requestResult.result);
    } else {
      return Result.error(requestResult.errorMessage);
    }
  }

  @override
  Future<Result<List<Relation>>> getListDemandesEnAttente(
      {User currentUser}) async {
    var requestResult = await dataSourceRelation.getListDemandesEnAttente(
        currentUser: currentUser);
    if (requestResult.isSuccess()) {
      return Result(requestResult.result);
    } else {
      return Result.error(requestResult.errorMessage);
    }
  }

  @override
  Future<Result<bool>> decisionRelation(
      {Relation relation, String token, EtatRelationApi etat}) async {
    var requestResult = await dataSourceRelation.decisionRelation(
        relation: relation, token: token,etat: etat);
    if (requestResult.isSuccess()) {
      return Result(requestResult.result);
    } else {
      return Result.error(requestResult.errorMessage);
    }
  }
}
