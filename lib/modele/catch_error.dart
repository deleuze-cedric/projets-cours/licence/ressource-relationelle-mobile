import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/modele/result.dart';
class CatchError {
  static Future<Result> handle(ObservableFuture observableFuture) async {
    try {
      await observableFuture;
      return observableFuture.result;
    } on Result catch (ex) {
      return ex;
    } catch (ex) {
      return Result.fromError(ex);
    }
  }
}