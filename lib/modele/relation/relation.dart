import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';



class Relation {
  String id;
  String type;
  TypeRelation typeRelation;
  User demandeur;
  User receveur;
  DateTime dateDemande;
  DateTime acceptedDate;

  Relation({
    this.id,
    this.type,
    this.typeRelation,
    this.demandeur,
    this.receveur,
    this.dateDemande,
    this.acceptedDate,
  });

  factory Relation.fromJson(Map<String, dynamic> json) {
    return Relation(
      id: json['@id'] as String,
      type: json['type'] as String,
      typeRelation: json['typeRelation'] == null
          ? null
          : TypeRelation.fromJson(json['typeRelation'] as Map<String, dynamic>),
      demandeur: json['demandeur'] == null
          ? null
          : User.fromJson(json['demandeur'] as Map<String, dynamic>),
      receveur: json['receveur'] == null
          ? null
          : User.fromJson(json['receveur'] as Map<String, dynamic>),
      dateDemande: json['dateDemande'] == null
          ? null
          : DateTime.parse(json['dateDemande'] as String),
      acceptedDate: json['acceptedDate'] == null
          ? null
          : DateTime.parse(json['acceptedDate'] as String),
    );
  }

  Map<String, dynamic>toJson (Relation instance) => <String, dynamic>{
    '@id': instance.id,
    'type': instance.type,
    'typeRelation': instance.typeRelation,
    'demandeur': instance.demandeur,
    'receveur': instance.receveur,
    'dateDemande': instance.dateDemande?.toIso8601String(),
    'acceptedDate': instance.acceptedDate?.toIso8601String(),
  };
}
