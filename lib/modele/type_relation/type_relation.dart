class TypeRelation {
  String id;
  String libelle;
  String slug;

  TypeRelation({this.id,this.libelle, this.slug});

  factory TypeRelation.fromJson(Map<String, dynamic> json){
    return TypeRelation(
      id: json['@id'] as String,
      libelle: json['libelle'] as String,
      slug: json['slug'] as String,
    );
  }
  Map<String, dynamic> toJson(TypeRelation instance) =>
      <String, dynamic>{
        '@id': instance.id,
        'libelle': instance.libelle,
        'slug': instance.slug,
      };
}
