class User{
  String id;
  String uuid;
  String email;
  String nom;
  String prenom;
  String pseudo;
  String password;
  String token;

  User({this.id,this.uuid,this.email, this.nom, this.prenom, this.pseudo, this.password});

  factory User.fromJson(Map<String,dynamic> json){
    return User(
      id: json['@id'] as String,
      uuid: json['uuid'] as String,
      email: json['email'] as String,
      nom: json['nom'] as String,
      prenom: json['prenom'] as String,
      pseudo: json['pseudo'] as String,
      password: json['password'] as String,
    )..token = json['token'] as String;
  }
  Map<String,dynamic> toJson(User instance) => <String, dynamic>{
    'id': instance.id,
    'uuid': instance.uuid,
    'email': instance.email,
    'nom': instance.nom,
    'prenom': instance.prenom,
    'pseudo': instance.pseudo,
    'password': instance.password,
    'token': instance.token,
  };
}