import 'package:json_annotation/json_annotation.dart';

part 'etat_relation_api.g.dart';

@JsonSerializable()
class EtatRelationApi{
  bool active;

  EtatRelationApi(this.active);

  factory EtatRelationApi.fromJson(Map<String, dynamic> json) =>
      _$EtatRelationApiFromJson(json);

  Map<String, dynamic> toJson() => _$EtatRelationApiToJson(this);
}