// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'etat_relation_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EtatRelationApi _$EtatRelationApiFromJson(Map<String, dynamic> json) {
  return EtatRelationApi(
    json['active'] as bool,
  );
}

Map<String, dynamic> _$EtatRelationApiToJson(EtatRelationApi instance) =>
    <String, dynamic>{
      'active': instance.active,
    };
