import 'package:json_annotation/json_annotation.dart';

part 'ressource_api.g.dart';

@JsonSerializable()
class RessourceApi{
  String titre;
  String contenu;
  String typeRessource;
  String categorieRessource;

  RessourceApi({this.titre,this.contenu, this.typeRessource, this.categorieRessource});

  factory RessourceApi.fromJson(Map<String, dynamic> json) =>
      _$RessourceApiFromJson(json);

  Map<String, dynamic> toJson() => _$RessourceApiToJson(this);
}