// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelationApi _$RelationApiFromJson(Map<String, dynamic> json) {
  return RelationApi(
    id: json['id'] as String,
    type: json['type'] as String,
    typeRelation: json['typeRelation'] as String,
    demandeur: json['demandeur'] as String,
    receveur: json['receveur'] as String,
    dateDemande: json['dateDemande'] == null
        ? null
        : DateTime.parse(json['dateDemande'] as String),
    acceptedDate: json['acceptedDate'] == null
        ? null
        : DateTime.parse(json['acceptedDate'] as String),
  );
}

Map<String, dynamic> _$RelationApiToJson(RelationApi instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'typeRelation': instance.typeRelation,
      'demandeur': instance.demandeur,
      'receveur': instance.receveur,
      'dateDemande': instance.dateDemande?.toIso8601String(),
      'acceptedDate': instance.acceptedDate?.toIso8601String(),
    };
