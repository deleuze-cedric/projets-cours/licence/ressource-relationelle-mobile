import 'package:json_annotation/json_annotation.dart';

part 'relation_api.g.dart';

@JsonSerializable()
class RelationApi {
  String id;
  String type;
  String typeRelation;
  String demandeur;
  String receveur;
  DateTime dateDemande;
  DateTime acceptedDate;

  RelationApi({
    this.id,
    this.type,
    this.typeRelation,
    this.demandeur,
    this.receveur,
    this.dateDemande,
    this.acceptedDate,
  });

  factory RelationApi.fromJson(Map<String, dynamic> json) =>
      _$RelationApiFromJson(json);

  Map<String, dynamic> toJson() => _$RelationApiToJson(this);
}
