// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ressource_form_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RessourceFormController on _RessourceFormController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_RessourceFormController.isLoading'))
          .value;

  final _$typeRessourceIdAtom =
      Atom(name: '_RessourceFormController.typeRessourceId');

  @override
  String get typeRessourceId {
    _$typeRessourceIdAtom.reportRead();
    return super.typeRessourceId;
  }

  @override
  set typeRessourceId(String value) {
    _$typeRessourceIdAtom.reportWrite(value, super.typeRessourceId, () {
      super.typeRessourceId = value;
    });
  }

  final _$categorieRessourceIdAtom =
      Atom(name: '_RessourceFormController.categorieRessourceId');

  @override
  String get categorieRessourceId {
    _$categorieRessourceIdAtom.reportRead();
    return super.categorieRessourceId;
  }

  @override
  set categorieRessourceId(String value) {
    _$categorieRessourceIdAtom.reportWrite(value, super.categorieRessourceId,
        () {
      super.categorieRessourceId = value;
    });
  }

  final _$errorAtom = Atom(name: '_RessourceFormController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_RessourceFormController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_RessourceFormController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_RessourceFormController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  @override
  String toString() {
    return '''
typeRessourceId: ${typeRessourceId},
categorieRessourceId: ${categorieRessourceId},
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
requestFuture: ${requestFuture},
isLoading: ${isLoading}
    ''';
  }
}
