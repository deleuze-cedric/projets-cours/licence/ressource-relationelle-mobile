// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'navigation_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NavigationController on _NavigationController, Store {
  final _$tabItemSelectedAtom =
      Atom(name: '_NavigationController.tabItemSelected');

  @override
  int get tabItemSelected {
    _$tabItemSelectedAtom.reportRead();
    return super.tabItemSelected;
  }

  @override
  set tabItemSelected(int value) {
    _$tabItemSelectedAtom.reportWrite(value, super.tabItemSelected, () {
      super.tabItemSelected = value;
    });
  }

  final _$listPagesAtom = Atom(name: '_NavigationController.listPages');

  @override
  List<Widget> get listPages {
    _$listPagesAtom.reportRead();
    return super.listPages;
  }

  @override
  set listPages(List<Widget> value) {
    _$listPagesAtom.reportWrite(value, super.listPages, () {
      super.listPages = value;
    });
  }

  final _$_NavigationControllerActionController =
      ActionController(name: '_NavigationController');

  @override
  void changeTabSelected(int index) {
    final _$actionInfo = _$_NavigationControllerActionController.startAction(
        name: '_NavigationController.changeTabSelected');
    try {
      return super.changeTabSelected(index);
    } finally {
      _$_NavigationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
tabItemSelected: ${tabItemSelected},
listPages: ${listPages}
    ''';
  }
}
