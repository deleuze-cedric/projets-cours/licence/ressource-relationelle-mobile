import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/type_relation/type_relation.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';

part 'type_relation_controller.g.dart';

class TypeRelationController = _TypeRelationController
    with _$TypeRelationController;

abstract class _TypeRelationController with Store {
  RepositoryRelation repositoryRelation;

  _TypeRelationController(this.repositoryRelation){
    getListTypeRelation();
  }

  @observable
  List<TypeRelation> listeTypeRelation = List();

  @observable
  bool error;

  @observable
  String message = "";

  @observable
  String errorMessage;

  @observable
  ObservableFuture requestFuture;

  @observable
  TypeRelation selectedTypeRelation = TypeRelation();

  @computed
  bool get isLoading {
    return requestFuture?.status == FutureStatus.pending;
  }

  @action
  Future<bool> getListTypeRelation() async {
    error = false;
    requestFuture = ObservableFuture(repositoryRelation.getListTypeRelation());
    var requestResult = await CatchError.handle(requestFuture);
    if (requestResult.isSuccess()) {
      error = false;
      listeTypeRelation = requestResult.result;
      selectedTypeRelation = listeTypeRelation.first;
      return error;
    } else {
      error = true;
      errorMessage = requestResult.errorMessage;
      return error;
    }
  }

  @action
  changeSelectedItem(TypeRelation newTypeRelation) {
    selectedTypeRelation = newTypeRelation;
  }
}
