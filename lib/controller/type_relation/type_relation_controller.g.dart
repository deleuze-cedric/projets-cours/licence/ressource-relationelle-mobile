// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_relation_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TypeRelationController on _TypeRelationController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_TypeRelationController.isLoading'))
          .value;

  final _$listeTypeRelationAtom =
      Atom(name: '_TypeRelationController.listeTypeRelation');

  @override
  List<TypeRelation> get listeTypeRelation {
    _$listeTypeRelationAtom.reportRead();
    return super.listeTypeRelation;
  }

  @override
  set listeTypeRelation(List<TypeRelation> value) {
    _$listeTypeRelationAtom.reportWrite(value, super.listeTypeRelation, () {
      super.listeTypeRelation = value;
    });
  }

  final _$errorAtom = Atom(name: '_TypeRelationController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_TypeRelationController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_TypeRelationController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_TypeRelationController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  final _$selectedTypeRelationAtom =
      Atom(name: '_TypeRelationController.selectedTypeRelation');

  @override
  TypeRelation get selectedTypeRelation {
    _$selectedTypeRelationAtom.reportRead();
    return super.selectedTypeRelation;
  }

  @override
  set selectedTypeRelation(TypeRelation value) {
    _$selectedTypeRelationAtom.reportWrite(value, super.selectedTypeRelation,
        () {
      super.selectedTypeRelation = value;
    });
  }

  final _$getListTypeRelationAsyncAction =
      AsyncAction('_TypeRelationController.getListTypeRelation');

  @override
  Future<bool> getListTypeRelation() {
    return _$getListTypeRelationAsyncAction
        .run(() => super.getListTypeRelation());
  }

  final _$_TypeRelationControllerActionController =
      ActionController(name: '_TypeRelationController');

  @override
  dynamic changeSelectedItem(TypeRelation newTypeRelation) {
    final _$actionInfo = _$_TypeRelationControllerActionController.startAction(
        name: '_TypeRelationController.changeSelectedItem');
    try {
      return super.changeSelectedItem(newTypeRelation);
    } finally {
      _$_TypeRelationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listeTypeRelation: ${listeTypeRelation},
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
requestFuture: ${requestFuture},
selectedTypeRelation: ${selectedTypeRelation},
isLoading: ${isLoading}
    ''';
  }
}
