// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'actualite_connect_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ActualiteConnectController on _ActualiteConnectController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_ActualiteConnectController.isLoading'))
          .value;

  final _$messageAtom = Atom(name: '_ActualiteConnectController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorAtom = Atom(name: '_ActualiteConnectController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_ActualiteConnectController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$requestListFutureAtom =
      Atom(name: '_ActualiteConnectController.requestListFuture');

  @override
  ObservableFuture<dynamic> get requestListFuture {
    _$requestListFutureAtom.reportRead();
    return super.requestListFuture;
  }

  @override
  set requestListFuture(ObservableFuture<dynamic> value) {
    _$requestListFutureAtom.reportWrite(value, super.requestListFuture, () {
      super.requestListFuture = value;
    });
  }

  final _$listRessourceAtom =
      Atom(name: '_ActualiteConnectController.listRessource');

  @override
  List<Ressource> get listRessource {
    _$listRessourceAtom.reportRead();
    return super.listRessource;
  }

  @override
  set listRessource(List<Ressource> value) {
    _$listRessourceAtom.reportWrite(value, super.listRessource, () {
      super.listRessource = value;
    });
  }

  final _$getListRessourceConnectAsyncAction =
      AsyncAction('_ActualiteConnectController.getListRessourceConnect');

  @override
  Future<void> getListRessourceConnect(String token) {
    return _$getListRessourceConnectAsyncAction
        .run(() => super.getListRessourceConnect(token));
  }

  @override
  String toString() {
    return '''
message: ${message},
error: ${error},
errorMessage: ${errorMessage},
requestListFuture: ${requestListFuture},
listRessource: ${listRessource},
isLoading: ${isLoading}
    ''';
  }
}
