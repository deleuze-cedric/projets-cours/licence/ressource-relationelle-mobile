import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/ressource/ressource.dart';
import 'package:ressource_relationnel/main.i18n.dart';
part 'actualite_connect_controller.g.dart';

class ActualiteConnectController = _ActualiteConnectController with _$ActualiteConnectController;

abstract class _ActualiteConnectController with Store{
  RepositoryRessource repositoryRessource;

  _ActualiteConnectController(repositoryRessource){
    this.repositoryRessource = repositoryRessource;
    //getListRessource();
  }

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  bool error=false;

  @observable
  String errorMessage;

  @observable
  ObservableFuture requestListFuture;

  @observable
  List<Ressource> listRessource = List<Ressource>();

  @computed
  bool get isLoading{
    return requestListFuture?.status == FutureStatus.pending;
  }

  @action
  Future<void> getListRessourceConnect(String token) async{
    error = false;
    requestListFuture =
        ObservableFuture(repositoryRessource.getListRessourceConnect(token));
    var listResult = await CatchError.handle(requestListFuture);
    if (listResult.isSuccess()) {
      listRessource = listResult.result;
    } else {
      error = true;
      //errorMessage = listResult.errorMessage;
      errorMessage = listResult.errorMessage;
    }
  }

}