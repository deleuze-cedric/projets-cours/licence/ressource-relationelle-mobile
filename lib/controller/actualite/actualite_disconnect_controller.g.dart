// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'actualite_disconnect_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ActualiteDisonnectController on _ActualiteDisconnectController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_ActualiteDisconnectController.isLoading'))
          .value;

  final _$listRessourceFutureAtom =
      Atom(name: '_ActualiteDisconnectController.listRessourceFuture');

  @override
  ObservableFuture<dynamic> get listRessourceFuture {
    _$listRessourceFutureAtom.reportRead();
    return super.listRessourceFuture;
  }

  @override
  set listRessourceFuture(ObservableFuture<dynamic> value) {
    _$listRessourceFutureAtom.reportWrite(value, super.listRessourceFuture, () {
      super.listRessourceFuture = value;
    });
  }

  final _$listRessourceAtom =
      Atom(name: '_ActualiteDisconnectController.listRessource');

  @override
  List<dynamic> get listRessource {
    _$listRessourceAtom.reportRead();
    return super.listRessource;
  }

  @override
  set listRessource(List<dynamic> value) {
    _$listRessourceAtom.reportWrite(value, super.listRessource, () {
      super.listRessource = value;
    });
  }

  final _$errorAtom = Atom(name: '_ActualiteDisconnectController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$messageAtom = Atom(name: '_ActualiteDisconnectController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_ActualiteDisconnectController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$getListRessourceAsyncAction =
      AsyncAction('_ActualiteDisconnectController.getListRessource');

  @override
  Future<void> getListRessource() {
    return _$getListRessourceAsyncAction.run(() => super.getListRessource());
  }

  @override
  String toString() {
    return '''
listRessourceFuture: ${listRessourceFuture},
listRessource: ${listRessource},
error: ${error},
message: ${message},
errorMessage: ${errorMessage},
isLoading: ${isLoading}
    ''';
  }
}
