// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ressource_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RessourceController on _RessourceController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_RessourceController.isLoading'))
          .value;

  final _$errorAtom = Atom(name: '_RessourceController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_RessourceController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$messageAtom = Atom(name: '_RessourceController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$listRessourceAtom = Atom(name: '_RessourceController.listRessource');

  @override
  List<dynamic> get listRessource {
    _$listRessourceAtom.reportRead();
    return super.listRessource;
  }

  @override
  set listRessource(List<dynamic> value) {
    _$listRessourceAtom.reportWrite(value, super.listRessource, () {
      super.listRessource = value;
    });
  }

  final _$requestFutureAtom = Atom(name: '_RessourceController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  @override
  String toString() {
    return '''
error: ${error},
errorMessage: ${errorMessage},
message: ${message},
listRessource: ${listRessource},
requestFuture: ${requestFuture},
isLoading: ${isLoading}
    ''';
  }
}
