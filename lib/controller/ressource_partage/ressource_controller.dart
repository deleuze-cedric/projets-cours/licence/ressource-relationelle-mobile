import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';

import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

part 'ressource_controller.g.dart';

class RessourceController = _RessourceController with _$RessourceController;

abstract class _RessourceController with Store {
  RepositoryRessource repositoryRessource;
  User currentUser;
  User otherUser;

  _RessourceController({repositoryRessource, currentUser, otherUser}) {
    this.repositoryRessource = repositoryRessource;
    this.currentUser = currentUser;
    this.otherUser = otherUser;
    getListRessourceDeUser();
  }

  @observable
  bool error = false;

  @observable
  String errorMessage = "";

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  List listRessource = List();

  @observable
  ObservableFuture requestFuture;

  @computed
  bool get isLoading {
    return requestFuture?.status == FutureStatus.pending;
  }

  Future getListRessourceDeUser() async {
    error = false;
    print("other user ${otherUser?.uuid}");
    print("current user token => ${currentUser?.token}");
    otherUser == null
        ? requestFuture = ObservableFuture(
            repositoryRessource.getListRessourceDeUser(currentUser, null))
        : requestFuture = ObservableFuture(
            repositoryRessource.getListRessourceDeUser(currentUser, otherUser));
    var requestResult = await CatchError.handle(requestFuture);
    if (requestResult.isSuccess()) {
      error = false;
      listRessource = requestResult.result;
      print("controller ${listRessource.length} => taille || element");
      return error;
    } else {
      error = true;
      errorMessage = requestResult.errorMessage;
      return error;
    }
  }
}
