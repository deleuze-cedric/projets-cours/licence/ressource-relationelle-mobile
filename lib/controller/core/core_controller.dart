import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

part 'core_controller.g.dart';

class CoreController = _CoreController with _$CoreController;

abstract class _CoreController with Store {
  _CoreController(repositoryUser);

  RepositoryUser repositoryUser;

  @observable
  bool isConnect = false;

  @observable
  User user= User();

  @action
   setIsConnect(bool newIsConnect) {
     this.isConnect = newIsConnect;
  }

  @action
  setUser(User newUser){
    user = newUser;
  }

}