// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_form_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserFormController on _UserFormController, Store {
  final _$userTokenAtom = Atom(name: '_UserFormController.userToken');

  @override
  String get userToken {
    _$userTokenAtom.reportRead();
    return super.userToken;
  }

  @override
  set userToken(String value) {
    _$userTokenAtom.reportWrite(value, super.userToken, () {
      super.userToken = value;
    });
  }

  final _$userAtom = Atom(name: '_UserFormController.user');

  @override
  User get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$emailAtom = Atom(name: '_UserFormController.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_UserFormController.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$msgAtom = Atom(name: '_UserFormController.msg');

  @override
  String get msg {
    _$msgAtom.reportRead();
    return super.msg;
  }

  @override
  set msg(String value) {
    _$msgAtom.reportWrite(value, super.msg, () {
      super.msg = value;
    });
  }

  final _$errorMsgAtom = Atom(name: '_UserFormController.errorMsg');

  @override
  String get errorMsg {
    _$errorMsgAtom.reportRead();
    return super.errorMsg;
  }

  @override
  set errorMsg(String value) {
    _$errorMsgAtom.reportWrite(value, super.errorMsg, () {
      super.errorMsg = value;
    });
  }

  final _$errorAtom = Atom(name: '_UserFormController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$authentificationFutureAtom =
      Atom(name: '_UserFormController.authentificationFuture');

  @override
  ObservableFuture<dynamic> get authentificationFuture {
    _$authentificationFutureAtom.reportRead();
    return super.authentificationFuture;
  }

  @override
  set authentificationFuture(ObservableFuture<dynamic> value) {
    _$authentificationFutureAtom
        .reportWrite(value, super.authentificationFuture, () {
      super.authentificationFuture = value;
    });
  }

  final _$registerAsyncAction = AsyncAction('_UserFormController.register');

  @override
  Future<dynamic> register(FormBuilderState state) {
    return _$registerAsyncAction.run(() => super.register(state));
  }

  @override
  String toString() {
    return '''
userToken: ${userToken},
user: ${user},
email: ${email},
password: ${password},
msg: ${msg},
errorMsg: ${errorMsg},
error: ${error},
authentificationFuture: ${authentificationFuture}
    ''';
  }
}
