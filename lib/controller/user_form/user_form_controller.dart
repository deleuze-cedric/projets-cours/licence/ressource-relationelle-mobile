import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/user/repository_user.dart';
import 'package:ressource_relationnel/modele/user/user.dart';
import 'package:ressource_relationnel/main.i18n.dart';
part 'user_form_controller.g.dart';


class UserFormController = _UserFormController with _$UserFormController;

abstract class _UserFormController with Store{
  RepositoryUser repositoryUser;


  _UserFormController(this.repositoryUser);

  @observable
  String userToken;

  @observable
  User user = User();

  @observable
  String email;

  @observable
  String password;

  @observable
  String msg = "Connexion en cours...".i18n;

  @observable
  String errorMsg = "";

  @observable
  bool error = false;

  @observable
  ObservableFuture authentificationFuture;

  @action
  Future register(FormBuilderState state) async {

    if(state.value["mdp"] == state.value["verif_mdp"]) {
      user.pseudo = state.value["pseudo"];
      user.nom = state.value["nom"];
      user.prenom = state.value["prenom"];
      user.email = state.value["mail"];
      user.password = state.value["mdp"];

      print(user.nom);
      print(user.prenom);
      print(user.pseudo);
      print(user.email);
      print(user.password);

      user = await repositoryUser.register(user);
    }else{
      print("ERREUR :  Les mots de passe sont différents !");
    }
  }
}