import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/main.i18n.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

part 'mes_demandes_controller.g.dart';

class MesDemandesController = _MesDemandesController with _$MesDemandesController;

abstract class _MesDemandesController with Store{
  RepositoryRelationAbstract repositoryRelationAbstract;
  User currentUser;

  _MesDemandesController(this.repositoryRelationAbstract,this.currentUser){
    getListMesDemandes();
  }

  @observable
  bool error = false;

  @observable
  String errorMessage;

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  ObservableFuture requestFuture;

  @observable
  ObservableFuture deleteFuture;

  @observable
  ObservableList<Relation> listRelation = ObservableList<Relation>();


  @computed
  bool get isLoading{
    return requestFuture?.status == FutureStatus.pending;
  }





  Future<bool> getListMesDemandes() async {
    error = false;
    requestFuture = ObservableFuture(repositoryRelationAbstract.getListMesDemandes(currentUser : currentUser));
    var listRelationResult = await CatchError.handle(requestFuture);
    if(listRelationResult.isSuccess()){
      error = false;
      listRelation = ObservableList.of(listRelationResult.result);
      print(listRelation.length);
      return error;
    }else{
      error = true;
      errorMessage = listRelationResult.errorMessage;
      return error;
    }
  }

  @action
  Future<bool> effacerDemande(Relation relation) async{
    deleteFuture = ObservableFuture(repositoryRelationAbstract.effacerRelation(relation : relation, token : currentUser.token));
    var deleteResult = await CatchError.handle(deleteFuture);
    if(deleteResult.isSuccess()){
      listRelation.removeWhere((element) => element.id == relation.id);
      return true;
    }else{
      return false;
    }

  }




}