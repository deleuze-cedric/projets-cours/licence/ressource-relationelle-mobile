// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mes_demandes_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MesDemandesController on _MesDemandesController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_MesDemandesController.isLoading'))
          .value;

  final _$errorAtom = Atom(name: '_MesDemandesController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_MesDemandesController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$messageAtom = Atom(name: '_MesDemandesController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_MesDemandesController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  final _$deleteFutureAtom = Atom(name: '_MesDemandesController.deleteFuture');

  @override
  ObservableFuture<dynamic> get deleteFuture {
    _$deleteFutureAtom.reportRead();
    return super.deleteFuture;
  }

  @override
  set deleteFuture(ObservableFuture<dynamic> value) {
    _$deleteFutureAtom.reportWrite(value, super.deleteFuture, () {
      super.deleteFuture = value;
    });
  }

  final _$listRelationAtom = Atom(name: '_MesDemandesController.listRelation');

  @override
  ObservableList<Relation> get listRelation {
    _$listRelationAtom.reportRead();
    return super.listRelation;
  }

  @override
  set listRelation(ObservableList<Relation> value) {
    _$listRelationAtom.reportWrite(value, super.listRelation, () {
      super.listRelation = value;
    });
  }

  final _$effacerDemandeAsyncAction =
      AsyncAction('_MesDemandesController.effacerDemande');

  @override
  Future<bool> effacerDemande(Relation relation) {
    return _$effacerDemandeAsyncAction
        .run(() => super.effacerDemande(relation));
  }

  @override
  String toString() {
    return '''
error: ${error},
errorMessage: ${errorMessage},
message: ${message},
requestFuture: ${requestFuture},
deleteFuture: ${deleteFuture},
listRelation: ${listRelation},
isLoading: ${isLoading}
    ''';
  }
}
