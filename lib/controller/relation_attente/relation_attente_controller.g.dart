// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_attente_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RelationAttenteController on _RelationAttenteController, Store {
  Computed<bool> _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: '_RelationAttenteController.isLoading'))
          .value;

  final _$errorAtom = Atom(name: '_RelationAttenteController.error');

  @override
  bool get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(bool value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$errorMessageAtom =
      Atom(name: '_RelationAttenteController.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$messageAtom = Atom(name: '_RelationAttenteController.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$requestFutureAtom =
      Atom(name: '_RelationAttenteController.requestFuture');

  @override
  ObservableFuture<dynamic> get requestFuture {
    _$requestFutureAtom.reportRead();
    return super.requestFuture;
  }

  @override
  set requestFuture(ObservableFuture<dynamic> value) {
    _$requestFutureAtom.reportWrite(value, super.requestFuture, () {
      super.requestFuture = value;
    });
  }

  final _$decisionFutureAtom =
      Atom(name: '_RelationAttenteController.decisionFuture');

  @override
  ObservableFuture<dynamic> get decisionFuture {
    _$decisionFutureAtom.reportRead();
    return super.decisionFuture;
  }

  @override
  set decisionFuture(ObservableFuture<dynamic> value) {
    _$decisionFutureAtom.reportWrite(value, super.decisionFuture, () {
      super.decisionFuture = value;
    });
  }

  final _$listRelationAtom =
      Atom(name: '_RelationAttenteController.listRelation');

  @override
  ObservableList<Relation> get listRelation {
    _$listRelationAtom.reportRead();
    return super.listRelation;
  }

  @override
  set listRelation(ObservableList<Relation> value) {
    _$listRelationAtom.reportWrite(value, super.listRelation, () {
      super.listRelation = value;
    });
  }

  final _$decisionDemandeAsyncAction =
      AsyncAction('_RelationAttenteController.decisionDemande');

  @override
  Future<bool> decisionDemande({Relation relation, EtatRelationApi etat}) {
    return _$decisionDemandeAsyncAction
        .run(() => super.decisionDemande(relation: relation, etat: etat));
  }

  @override
  String toString() {
    return '''
error: ${error},
errorMessage: ${errorMessage},
message: ${message},
requestFuture: ${requestFuture},
decisionFuture: ${decisionFuture},
listRelation: ${listRelation},
isLoading: ${isLoading}
    ''';
  }
}
