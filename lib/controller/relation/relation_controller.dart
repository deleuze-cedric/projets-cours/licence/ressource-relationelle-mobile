import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/relation/repository_relation.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/relation/relation.dart';
import 'package:ressource_relationnel/modele/user/user.dart';

import 'package:ressource_relationnel/main.i18n.dart';

part 'relation_controller.g.dart';

class RelationController = _RelationController with _$RelationController;

abstract class _RelationController with Store {
  RepositoryRelation repositoryRelation;
  User currentUser;
  _RelationController(repositoryRelation, currentUser){
    this.repositoryRelation = repositoryRelation;
    this.currentUser = currentUser;
    getListRelation();
  }

  @observable
  bool error = false;

  @observable
  String errorMessage;

  @observable
  String message = "Téléchargement...".i18n;

  @observable
  ObservableFuture requestFuture;

  @observable
  List<Relation> listRelation = List<Relation>();

  @computed
  bool get isLoading{
    return requestFuture?.status == FutureStatus.pending;
  }

  Future<bool> getListRelation() async {
    error = false;
    requestFuture = ObservableFuture(repositoryRelation.getListRelation(currentUser));
    var listRelationResult = await CatchError.handle(requestFuture);
    if(listRelationResult.isSuccess()){
      error = false;
      listRelation = listRelationResult.result;
      return error;
    }else{
      error = true;
      errorMessage = listRelationResult.errorMessage;
      return error;
    }
  }


}