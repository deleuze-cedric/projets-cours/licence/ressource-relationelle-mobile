import 'package:mobx/mobx.dart';
import 'package:ressource_relationnel/data/ressource/repository_ressource.dart';
import 'package:ressource_relationnel/modele/catch_error.dart';
import 'package:ressource_relationnel/modele/type_ressource/type_ressource.dart';



part 'type_ressource_controller.g.dart';

class TypeRessourceController = _TypeRessourceController with _$TypeRessourceController;

abstract class _TypeRessourceController with Store {
  RepositoryRessource repositoryRessource;
  _TypeRessourceController(repositoryRessource){
    this.repositoryRessource = repositoryRessource;
    getListTypeRessource();
  }

  @observable
  List<TypeRessource> listTypeRessource = new List();

  @observable
  String message;

  @observable
  bool error = false;

  @observable
  String errorMessage="";


  //Le choix du type de ressource qui va être crée
  @observable
  String idTypeRessSel = null;

  @observable
  ObservableFuture listTypeFuture;

  @computed
  bool get isLoading{
    return listTypeFuture?.status == FutureStatus.pending;
  }

  @action
  Future getListTypeRessource() async {
    error = false;
    message = "Téléchargement...";
    listTypeFuture =  ObservableFuture(repositoryRessource.getListTypeRessource());
    var listResult = await CatchError.handle(listTypeFuture);
    if (listResult.isSuccess()){
      listTypeRessource = listResult.result;
    }else{
      error = true;
      errorMessage = listResult.errorMessage;
    }
  }

  @action
  void setIdTypeRessSel(String newId){
    idTypeRessSel = newId;
    repositoryRessource.idTypeRessToCreate = idTypeRessSel;
  }




}