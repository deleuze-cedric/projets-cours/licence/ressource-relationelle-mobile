// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_navigation_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RelationNavigationController on _RelationNavigationController, Store {
  final _$tabItemSelectedAtom =
      Atom(name: '_RelationNavigationController.tabItemSelected');

  @override
  int get tabItemSelected {
    _$tabItemSelectedAtom.reportRead();
    return super.tabItemSelected;
  }

  @override
  set tabItemSelected(int value) {
    _$tabItemSelectedAtom.reportWrite(value, super.tabItemSelected, () {
      super.tabItemSelected = value;
    });
  }

  final _$_RelationNavigationControllerActionController =
      ActionController(name: '_RelationNavigationController');

  @override
  void changeTabSelected(int index) {
    final _$actionInfo = _$_RelationNavigationControllerActionController
        .startAction(name: '_RelationNavigationController.changeTabSelected');
    try {
      return super.changeTabSelected(index);
    } finally {
      _$_RelationNavigationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  Widget displayWidget() {
    final _$actionInfo = _$_RelationNavigationControllerActionController
        .startAction(name: '_RelationNavigationController.displayWidget');
    try {
      return super.displayWidget();
    } finally {
      _$_RelationNavigationControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
tabItemSelected: ${tabItemSelected}
    ''';
  }
}
